const {OPEN_READWRITE} = require("sqlite3");
let sqlite3 = require('sqlite3').verbose();

let db = new sqlite3.Database('./db.sqlite', OPEN_READWRITE, err => {
    if (err) console.error(err)
})

async function get_records(order = '', field = 'date', name = '') {
    let ret = []
    let rows
    if (name === '')
        rows = await get_db_records(order, field)
    else
        rows = await find_db_records(order, field, name)
    let new_obj
    for (let row of rows) {
        new_obj = {}
        new_obj['id'] = row['id']
        new_obj['header'] = row['header']
        new_obj['date'] = row['date']
        new_obj['body'] = row['body']
        new_obj['owner'] = row['owner_id']
        ret.push(new_obj)
    }
    return ret
}

function get_db_records(order, field) {
    order = order === 'desc' ? ' DESC' : ''
    return new Promise((resolve, reject) => {
        db.serialize(() => {
            db.all(`SELECT *
                    FROM records
                    ORDER BY ${field}${order}`, (err, rows) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(rows)
                }
            })
        })
    })
}

function find_db_records(order, field, name) {
    order = order === 'desc' ? ' DESC' : ''
    return new Promise((resolve, reject) => {
        db.serialize(() => {
            db.all(`SELECT *
                    FROM records
                    WHERE (header LIKE '%${name}%'
                        OR body LIKE '%${name}%'
                        OR date LIKE '%${name}%') COLLATE NOCASE
                    ORDER BY ${field}${order}`, (err, rows) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(rows)
                }
            })
        })
    })
}

async function add_db_record(date, header, body, owner_id = 0) {
    db.run('INSERT INTO records(header, date, body, owner_id) VALUES(?, ?, ?, ?)', [header, date, body, owner_id], (err) => {
        if (err) console.error(err);
    })
}

async function edit_db_record(id, date, header, body) {
    db.run(`UPDATE records
            SET date = ?,
                header = ?,
                body = ?
            WHERE id = ?`, [date, header, body, id], (err) => {
        if (err) console.error(err);
    })
}

async function delete_db_record(id) {
    db.run('DELETE FROM records WHERE id = ?', [id], (err) => {
        if (err) console.error(err);
    })
}

async function get_user(username) {
    let rows = await get_db_users(username)
    if (rows)
        return {'id': rows['id'], 'username': rows['username'], 'password': rows['password']}
    else return null
}

async function get_db_users(username) {
    return new Promise((resolve, reject) => {
        db.serialize(() => {
            db.get(`SELECT *
                    FROM users
                    WHERE (users.username = ?) COLLATE NOCASE`, [username], (err, rows) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(rows)
                }
            })
        })
    })
}

async function add_user(username, password) {
    await db.run('INSERT INTO users(username, password) VALUES (?, ?)', [username, password], (err) => {
        if (err) console.error(err)
    })
}

async function get_user_access(target_id) {
    return new Promise((resolve, reject) => {
        db.serialize(() => {
            db.all('SELECT owner_id FROM access WHERE target_id = ?', [target_id], (err, rows) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(rows)
                }
            })
        })
    })
}

async function get_users_shared_access(owner_id) {
    return new Promise((resolve, reject) => {
        db.serialize(() => {
            db.all('SELECT target_id FROM access WHERE owner_id = ?', [owner_id], (err, rows) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(rows)
                }
            })
        })
    })
}

async function get_user_names_ids() {
    return new Promise((resolve, reject) => {
        db.serialize(() => {
            db.all('SELECT id, username, role FROM users', (err, rows) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(rows)
                }
            })
        })
    })
}

async function grant_access(owner_id, target_id) {
    await db.run('INSERT INTO access(owner_id, target_id) VALUES (?, ?)', [owner_id, target_id], (err) => {
        if (err) console.error(err)
    })
}

async function revoke_access(owner_id, target_id) {
    await db.run('DELETE FROM access WHERE owner_id = ? AND target_id = ?', [owner_id, target_id], (err) => {
        if (err) console.error(err)
    })
}

module.exports = {
    get_records,
    add_db_record,
    edit_db_record,
    delete_db_record,
    get_user,
    add_user,
    get_user_access,
    get_user_names_ids,
    get_users_shared_access,
    grant_access,
    revoke_access,
}