let express = require('express');
let router = express.Router();
let multer = require('multer');
let db = require('../tools/db_interaction')


// 1
router.get('/1', async function (req, res) {
    let data, sorting = false
    if (req.query) {
        data = await db.get_records(req.query['order'], req.query['field'], req.query['name']);
        sorting = {'order': req.query['order'], 'field': req.query['field'], 'name': req.query['name']}
    } else data = await db.get_records()
    res.render('1', {title: 'Дорогой дневник 1', data: data ? data : [], sorting: sorting})
});

router.post('/1', multer().none(), async function (req, res) {
    if (req.body['type'] === 'create')
        await db.add_db_record(req.body['date'], req.body['header'], req.body['body'])
    else if (req.body['type'] === 'edit')
        await db.edit_db_record(req.body['id'], req.body['date'], req.body['header'], req.body['body'])
    else if (req.body['type'] === 'delete')
        await db.delete_db_record(req.body['id'])
    let data, sorting = false
    if (req.query) {
        data = await db.get_records(req.query['order'], req.query['field'], req.query['name']);
        sorting = {'order': req.query['order'], 'field': req.query['field'], 'name': req.query['name']}
    } else data = await db.get_records()
    res.render('1', {title: 'Дорогой дневник 1', data: data ? data : [], sorting: sorting})
})

// 2
router.get('/2', function (req, res) {
    res.render('2', {title: 'Дорогой дневник 2'})
})

//3
router.get('/3', function (req, res) {
    res.render('3', {title: 'Дорогой дневник 3'})
})

module.exports = router;
