let express = require('express');
let router = express.Router();
let db = require('../tools/db_interaction');
let jwt = require('jsonwebtoken')

let TOKEN = '09f26e402586e2faa8da4c98a35f1b20d6b033c6097befa8be3486a829587fe2f90a832bd3ff9d42710a4da095a2ce285b009f0c3730cd9b8e1af3eb84df6611'

function authenticateToken(req, res, next) {
    const token = req.headers['authorization']

    if (token == null) { return res.sendStatus(401)}
    jwt.verify(token, TOKEN, (err, user) => {
        if (err) {return res.sendStatus(403)}

        req.user = user['id']

        next()
    })
}

async function generateAccessToken(username, password) {
    let user = await db.get_user(username)
    if (user !== null && user['password'] === password)
        return jwt.sign({'id': user['id']}, TOKEN, {expiresIn: '30s'});
    else return '_'
}

router.post('/create/', async function (req, res) {
    await db.add_user(req.body['username'], req.body.password)
    res.sendStatus(200)
})

router.post('/login/', async function(req, res) {
    let token = await generateAccessToken(req.body['username'], req.body['password'])
    if (token !== '_')
        res.json(token)
    else
        res.sendStatus(403)
})

router.get('/users/', async function(req, res) {
    let username = req.query['username']
    let response = {'exists': false}
    if (await db.get_user(username))
        response['exists'] = true
    res.json(response)
})

router.post('/users/', async function(req, res) {
    await db.add_user(req.body['username'], req.body['password'])
    res.sendStatus(200)
})

router.get('/records/', authenticateToken, async function (req, res) {
    let data = await db.get_records()
    data = await filter_records(data, req.user)
    res.json(data)
})

router.delete('/records/', authenticateToken, async function(req, res) {
    let id = parseInt(req.body['id'])
    await db.delete_db_record(id)
    res.sendStatus(200)
})

router.put('/records/', authenticateToken, async function(req, res) {
    await db.edit_db_record(req.body['id'], req.body['date'], req.body['header'], req.body['body'])
    res.sendStatus(200)
})

router.post('/records/', authenticateToken, async function(req, res) {
    await db.add_db_record(req.body['date'], req.body['header'], req.body['body'])
    res.sendStatus(200)
})

router.get('/users/this/', authenticateToken, async function(req, res) {
    res.json({'id': req.user})
})

async function filter_records(data, user_id) {
    let user_access = await db.get_user_access(user_id);
    let tmp = await db.get_user_names_ids()
    let id_users = {}, users_id = {}
    let is_admin = false
    for (let user of tmp) {
        id_users[user.id] = user.username
        users_id[user.username] = user.id
        if (user_id === user.id && user.role === 'admin')
            is_admin = true
    }
    let usernames_to_add = ['Self']
    if (is_admin) {
        for (let user of tmp)
            if (user.id !== user_id)
                usernames_to_add.push(user.username)
    } else if (user_access)
        for (let entry of user_access)
            usernames_to_add.push(id_users[entry.owner_id])
    /*let ids = [user_id]
    if (is_admin) {
        for (let user of tmp)
            if (user.id !== user_id)
                ids.push(user.id)
    } else if (user_access)
        for (let entry of user_access)
            ids.push(entry['owner_id'])*/
    let ret = {}
    for (let username of usernames_to_add)
        if (username === id_users[user_id])
            ret['Self'] = []
        else ret[username] = []
    for (let rec of data)
        if (rec.owner === user_id)
            ret['Self'].push(rec)
        else if (ret[id_users[rec.owner]]) ret[id_users[rec.owner]].push(rec)
    /*for (let id of ids)
        ret[id] = []
    for (let rec of data) {
        rec.owner_name = users[rec.owner]
        if (user_id === rec.owner)
            rec.own = true
        if (ids.includes(rec.owner))
            ret[rec['owner']].push(rec)
    }*/
    return ret
}

module.exports = router;