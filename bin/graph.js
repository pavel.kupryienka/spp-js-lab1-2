let { graphqlHTTP } = require("express-graphql")
let { buildSchema } = require("graphql")
const {get_user_names_ids, get_users_shared_access} = require("../tools/db_interaction");


let schema = buildSchema(`
  type Query {
    access(user_id: Int): [Dict]
  }
  type Dict {
    id: Int
    username: String
    role: String
    added: Boolean
  }
`)

// The root provides a resolver function for each API endpoint
let root = {
    access: async (arg) => {
        let tmp = await get_user_names_ids()
        let access = await get_users_shared_access(arg.user_id)
        let ret = []
        let added = []
        for (let entry of access)
            added.push(entry.target_id)
        for (let entry of tmp) {
            if (added.includes(entry.id))
                entry['added'] = true
            ret.push(entry)
        }
        return ret
    },
}

let http = graphqlHTTP({
     schema: schema,
     rootValue: root,
     graphiql: true
})

module.exports = http