let server = require('./www')
const {Server} = require("socket.io");
const { grant_access, revoke_access } = require("../tools/db_interaction");

let io = new Server(server);

io.on('connection', (socket) => {
    console.log('a user connected');
    /*socket.on('hi', (msg) => {
        console.log(msg)
    })
    socket.on('bye', (_) => {
        io.emit('see', 'HEY YOU!')
    })*/
    socket.on('add_owner', async (arg) => {
        await grant_access(arg.owner, arg.target)
    })

    socket.on('delete_owner', async (arg) => {
        await revoke_access(arg.owner, arg.target)
    })
});

module.exports = io